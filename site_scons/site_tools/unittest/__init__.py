"""
Unit test using Unity + CMock
"""

import logging
import os
import subprocess
import re
import importlib

from SCons.Script import *

import fsops
from .include_parser import IncludeParser
import osops
from sources import Sources

path_variables = None


"""
SCons tools functions
"""


def generate(env):
    env.AddMethod(unittest_method, "Test")


def exists():
    return True


"""
Functions for determining Ruby version
"""


def get_ruby_version(print_ruby_version=False):
    """
    This function returns the Ruby version of the host machine, in the format of x.x.x
    """
    result = subprocess.run(['ruby', '--version'], capture_output=True, text=True)
    ruby_version = result.stdout.strip()
    ruby_version = re.search(r'ruby (\d+\.\d+\.\d+)', ruby_version).group(1)
    if print_ruby_version:
        print(ruby_version)
    return ruby_version

def compare_ruby_versions(host_ruby_version):
    """
    Since Ruby version 3.2.0, dir.exists? was removed, and replaced with dir.exists. 
    A check is implemented to make sure the code is compatible with the host's Ruby version. 
    A return value of 1 means the host's ruby version is newer than 3.2.0
    A return value of -1 means the host's ruby version is older than 3.2.0
    A return value of 0 means the host's ruby version is 3.2.0
    """
    TARGET_RUBY_VERSION = '3.2.0'

    TARGET_RUBY_VERSION_segments = list(map(int, TARGET_RUBY_VERSION.split('.')))
    host_ruby_version_segments = list(map(int, host_ruby_version.split('.')))

    for segment1, segment2 in zip(host_ruby_version_segments, TARGET_RUBY_VERSION_segments):
        if segment1 > segment2:
            return 1
        elif segment1 < segment2:
            return -1

    return 0

def import_the_needed_path(host_ruby_version_is_newer):
    sys.path.append(os.path.dirname(__file__))

    if host_ruby_version_is_newer == -1 :
        path_variables = importlib.import_module('unit_test_path_variables')
    else:
        path_variables = importlib.import_module('unit_test_path_variables_rb32')
    
    return path_variables

def import_the_correct_path(verbose=False):
    host_ruby_version = get_ruby_version()
    host_ruby_version_is_newer = compare_ruby_versions(host_ruby_version)
    path_variables = import_the_needed_path(host_ruby_version_is_newer)

    if verbose:
        print("Host Ruby version", host_ruby_version)
        print("Importing ", path_variables.__name__)

    return path_variables

"""
Environment functions
"""

def unittest_method(env, source, target, sources=None, prepend_include_dirnodes=None, summary_only=False, timeout=None, verbose=False):
    path_variables = import_the_correct_path()

    if verbose:
        summary_only = False

    all_exe_filenodes = []
    dependent_srcpath_objpath_map = {}
    env_ut = get_unittest_env(env)

    prepend_include_dirnodes = []
    for filenode in sources.unit_test_header_filenodes:
        if filenode.dir not in prepend_include_dirnodes:
            prepend_include_dirnodes.append(filenode.dir)
    env_ut.Prepend(CPPPATH=prepend_include_dirnodes)

    unittest_obj_filenodes = []
    for source_filenode in path_variables.SOURCE_FILES:
        unittest_obj_filenodes += env_ut.Object(target=fsops.ch_target_filenode(source_filenode, target.Dir(path_variables.OBJ_DIRNAME), "o"), source=source_filenode)

    for filenode_ut in source:
        output_dirnode = target.Dir(fsops.basename(filenode_ut))
        mock_output_dirnode = output_dirnode.Dir(path_variables.MOCK_DIRNAME)

        env_ut.Append(CPPPATH=Dir(filenode_ut.dir.abspath))
        env_ut.Append(CPPPATH=mock_output_dirnode)

        filenode_ut_main = generate_test_main(env, filenode_ut, target_dirnode=output_dirnode)

        obj_filenodes = []

        if sources is not None:
            dependent_source_filenodes, mock_header_filenodes = find_dependencies_from_sources(filenode_ut, sources, sources.unit_test_header_filenodes, verbose)
            for filenode in dependent_source_filenodes:
                if filenode not in dependent_srcpath_objpath_map:
                    objs = env_ut.Object(target=fsops.ch_target_filenode(filenode, target.Dir(path_variables.OBJ_DIRNAME), "o"), source=filenode)
                    dependent_srcpath_objpath_map[filenode] = objs[0]
                    obj_filenodes += objs
                else:
                    obj_filenodes.append(dependent_srcpath_objpath_map[filenode])

            _, mock_source_filenodes = generate_mocks(env_ut, mock_header_filenodes, mock_output_dirnode)
            for mock_source_filenode in mock_source_filenodes:
                obj_filenodes += env_ut.Object(target=fsops.ch_target_filenode(mock_source_filenode, output_dirnode.Dir(path_variables.OBJ_DIRNAME), "o"), source=mock_source_filenode)

        obj_filenodes += unittest_obj_filenodes
        obj_filenodes += env_ut.Object(target=fsops.ch_target_filenode(filenode_ut_main, output_dirnode.Dir(path_variables.OBJ_DIRNAME), "o"), source=filenode_ut_main)
        obj_filenodes += env_ut.Object(target=fsops.ch_target_filenode(filenode_ut, output_dirnode.Dir(path_variables.OBJ_DIRNAME), "o"), source=filenode_ut)

        exe_filenodes = env_ut.Program(target=fsops.ch_target_filenode(filenode_ut, output_dirnode, "exe"), source=obj_filenodes)
        all_exe_filenodes += exe_filenodes

    result = execute_unit_tests(env_ut, all_exe_filenodes, summary_only=summary_only, timeout=timeout)

    return result


"""
Helper methods
"""


def get_unittest_env(source_env):
    path_variables = import_the_correct_path()

    env_ut = source_env.Clone()
    env_ut.Append(
        CPPDEFINES=[
            "UNIT_TEST=1",
            "UNITY_OUTPUT_COLOR=1",
        ]
    )
    env_ut.Append(CPPPATH=path_variables.INCLUDE_DIRS)
    env_ut.Append(
        CFLAGS=[
            "-g",
            "-O0",
        ],
    )
    return env_ut

def get_unittest_coverage_env(source_env):
    env_ut = get_unittest_env(source_env)
    env_ut.Append(
        CPPFLAGS=[
            "-fprofile-arcs",
            "-ftest-coverage"
        ],
        LIBS=["gcov"],
    )
    return env_ut


def generate_test_main(env, filenode, target_dirnode):
    path_variables = import_the_correct_path()

    output_filenode = target_dirnode.File(fsops.suffix_filenode_name(filenode, suffix="_runner").name)
    return env.Command(action="ruby \"{}\" $SOURCE $TARGET".format(path_variables.GENERATE_TEST_RUNNER_RB.abspath), source=filenode, target=output_filenode)[0]


def find_dependencies_from_sources(filenode, sources, header_filenodes_override=None, verbose=False):
    path_variables = import_the_correct_path()

    include_parser = IncludeParser(filenode.abspath)
    dependent_source_filenodes = []
    missing_dependency_filenames = []

    mock_header_filenodes = []
    missing_mock_header_filenames = []

    for filename in include_parser.filenames:
        basename, _ = os.path.splitext(filename)
        if filename in path_variables.IGNORE_HEADER_FILENAME:
            continue

        if not filename.startswith(path_variables.MOCK_HEADER_PREFIX):
            for source_filenode in sources.source_filenodes:
                source_basename, _ = os.path.splitext(source_filenode.name)
                if basename == source_basename:
                    dependent_source_filenodes.append(source_filenode)
                    break
            else:
                missing_dependency_filenames.append(filename)
        else:  # filename.startswith(MOCK_HEADER_PREFIX)
            mock_header_filename = filename.replace(path_variables.MOCK_HEADER_PREFIX, "", 1)
            if header_filenodes_override is None:
                header_filenodes_override = []
            for header_filenode in header_filenodes_override + sources.include_filenodes:
                if mock_header_filename == header_filenode.name:
                    mock_header_filenodes.append(header_filenode)
                    break
            else:
                missing_mock_header_filenames.append(mock_header_filename)

    if (len(missing_dependency_filenames) > 0 or len(missing_mock_header_filenames) > 0) and verbose:
        print("WARNING: Missing dependencies for [{}]".format(filenode.name))
        for filename in missing_dependency_filenames:
            print("No matching source file for [{}]".format(filename))
        for filename in missing_mock_header_filenames:
            print("No header file [{}]".format(filename))
        print("")

    return dependent_source_filenodes, mock_header_filenodes


def generate_mocks(env, header_filenodes, target_dirnode):
    path_variables = import_the_correct_path()

    mock_header_filenodes = []
    mock_source_filenodes = []
    for header_filenode in header_filenodes:
        basename, ext = os.path.splitext(header_filenode.name)
        mock_header_filenode = target_dirnode.File("{}{}".format(path_variables.MOCK_HEADER_PREFIX, header_filenode.name))
        mock_source_filenode = target_dirnode.File("{}{}{}".format(path_variables.MOCK_HEADER_PREFIX, basename, ext.replace("h", "c")))
        results = env.Command(action="ruby \"{}\" $SOURCE \"{}\"".format(path_variables.MOCK_GENERATOR_RB.abspath, target_dirnode.abspath), source=header_filenode, target=[mock_header_filenode, mock_source_filenode])
        mock_header_filenodes.append(mock_header_filenode)
        mock_source_filenodes.append(mock_source_filenode)
    return mock_header_filenodes, mock_source_filenodes


def execute_unit_tests(env, exe_filenodes, summary_only=False, timeout=None):
    path_variables = import_the_correct_path()

    # Example:
    # python <UNIT_TEST_RUNNER_PY> -i <exe> -i <exe> -i <exe>
    command = [
        osops.get_python_exe(),
        "\"{}\"".format(path_variables.UNIT_TEST_RUNNER_PY.abspath),
    ]
    if summary_only:
        command.append("--summary-only")
    if timeout is not None:
        command.append("--timeout={}".format(str(timeout)))

    command.extend(map(lambda filenode: "-i \"{}\"".format(filenode.abspath), exe_filenodes))

    result = env.Command(target=None, source=exe_filenodes, action=" ".join(command))

    return result

