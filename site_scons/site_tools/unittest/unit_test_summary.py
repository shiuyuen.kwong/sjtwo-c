from collections import OrderedDict

from color import ColorString
from prettytable import PrettyTable

import re

class UnitTestSummary(object):
    COLUMNS = ["Name", "Result"]

    RESULT_PASSED = "PASSED"
    RESULT_FAILED = "FAILED"

    def __init__(self):
        self._unit_test_result_map = OrderedDict()

    def __len__(self):
        return len(self._unit_test_result_map)

    def __iter__(self):
        for item in self._unit_test_result_map.items():
            yield item

    def __str__(self):
        prettytable = PrettyTable(field_names=self.COLUMNS, title="Unit Test Operation Summary")
        for row in self:
            prettytable.add_row(row)
        return prettytable.get_string()

    """
    Public methods
    """
    def add_result(self, unit_test_name, success):
        """
        :param unit_test_name: Name of a unit test to display (str)
        :param success: True if the unit test execution is successful (bool)
        """
        result = self.RESULT_PASSED if success else self.RESULT_FAILED
        self._unit_test_result_map[unit_test_name] = self._get_result_string_colored(result)

    def prettytable_to_junit(self):
        color_pattern = re.compile(r'\x1b[^m]*m')

        rows = self.__str__().strip().split('\n')[5:]
        testcases = []
        for row in rows:
            cells = re.split(r'\s*\|\s*', row.strip())
            if len(cells) >= 2:
                name = re.sub(color_pattern, '', cells[1]).strip()
                result = re.sub(color_pattern, '', cells[2]).strip()
                testcases.append(self._create_testcase(name, result))

        test_suite = '\n'.join(testcases)
        junit_xml = f'''<testsuite name="Unit Test Operation Summary" tests="{len(testcases)}">{test_suite}</testsuite>'''
        return junit_xml

    """
    Private methods
    """
    def _get_result_string_colored(self, result):
        result_color_map = {
            self.RESULT_PASSED: ColorString(result).green,
            self.RESULT_FAILED: ColorString(result).red,
        }
        return result_color_map[result]
    
    def _create_testcase(self, name, result):
        testcase = f'  <testcase name="{name}">'
        if result != 'PASSED':
            failure_text = re.sub(r'\x1b[^m]*m', '', result)
            failure = f'    <failure>{failure_text}</failure>'
            testcase += failure
        testcase += '</testcase>'
        return testcase



    """
    Accessors
    """
    @property
    def has_failure(self):
        failure = False
        for _, result in self:
            if self.RESULT_FAILED in result:
                failure = True
                break
        return failure
