import os
from SCons.Script import *

SELF_DIR = Dir(os.path.dirname(__file__))
UNITY_DIR = SELF_DIR.Dir("throw_the_switch/unity/v2.5.0")
CMOCK_DIR = SELF_DIR.Dir("throw_the_switch/cmock/v2.5.0.rb32")
CEXCEPTION_DIR = SELF_DIR.Dir("throw_the_switch/cexception/v1.3.1")

SOURCE_FILES = [
    UNITY_DIR.File("src/unity.c"),
    CMOCK_DIR.File("src/cmock.c"),
    CEXCEPTION_DIR.File("lib/cexception.c")
]

INCLUDE_DIRS = [
    UNITY_DIR.Dir("src"),
    CMOCK_DIR.Dir("src"),
    CEXCEPTION_DIR.Dir("lib"),
]

""" Unity variables """
GENERATE_TEST_RUNNER_RB = UNITY_DIR.File("auto/generate_test_runner.rb")

""" CMock variables """
MOCK_GENERATOR_RB = CMOCK_DIR.File("scripts/create_mock.rb")
MOCK_HEADER_PREFIX = "Mock"
MOCK_DIRNAME = "mock"
IGNORE_HEADER_FILENAME = [
    "unity.h"
]

""" Unit test runner variables """
UNIT_TEST_RUNNER_PY = SELF_DIR.File("unit_test_runner.py")

""" Common variables """
OBJ_DIRNAME = "obj"
EXE_DIRNAME = "exe"

